import pika

# Cria conexão com broker localizado em localhost
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()


# Declara uma fila com o nome de 'hello'
# O comando de declaração de fila é idempotente,  ele garante que a fila irá existir.
# Assim, caso fila já exista nada acontece, mas caso fila NÃO exista ele cria a fila.
# OBS: Se rabbitmq tentar entregar msg para fila que não existe ele dropa a msg
channel.queue_declare(queue='hello')


# Publicar uma mensagem direcionada para a fila 'hello'
# No RabbitMQ uma msg nunca é entregue diretamente para fila, sempre precisa passar por um exchange.
# O exchange padrão é uma string vazia. Este exchange é especial pois permite especificar a fila desejada.
# Neste caso a fila deve ser informada no campo routing_key.
channel.basic_publish(
    exchange='',
    routing_key='hello',
    body='Hello World!'
)
print(" [x] Sent 'Hello World!'")


# Fechando a conexão
connection.close()
