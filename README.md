# Tutorial de RabbitMQ 1 - Hello World

Requisitos:
 - SO Linux (não testado em outros SOs)
 - Python 3
 - Docker


##  Criando ambiente
~~~bash
# Inicie um container de rabbitmq, ele será acessível em http://localhost:15672 com guest/guest
sudo docker run --name my_rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq:3-management

# Em outra aba/sessão de terminal, crie e ative um ambiente virtual python
python -m venv venv
source venv/bin/activate

# Crie um arquivo de requirements.txt indicando a lib pika e instale-a
echo 'pika' > requirements.txt
pip install -r requirements.txt
~~~


##  Criando producer de mensagens

Crie um arquivo [send.py](send.py) contendo o código de envio de msg para a fila.
Este código ao rodar envia uma msg e encerra.

## Validando o producer e o envio de mensagens

### Pelo navegador
1. Acesse <http://localhost:15672> com guest/guest. Lá você tem acesso ao painel do rabbitmq
2. Rode o código python do producer com o comando
~~~bash
python send.py
~~~
3. Confira no navegador as alterações no painel do rabbitmq, deve aparecer um gráfico indicando uma msg enfileirada.

### Pelo comando CLI

Após checar pelo navegador verifique também a situação por linha de comando
~~~bash
# Num servidor on-premise o comando seria "sudo rabbitmqctl list_queues"
sudo docker exec -ti my_rabbitmq bash -c "rabbitmqctl list_queues"
~~~

Retorno:
~~~
Listing queues for vhost / ...
name    messages
hello   1
~~~

## Criando o consumer de mensagens

Crie um arquivo [receive.py](receive.py) contendo o código consumo de msgs da fila.
Este código roda em loop eterno procurando por msgs para consumir.


## Validando o consumer e o consumo de mensagens

~~~bash
python receive.py
~~~
Você verá na tela o que consumer estará rodando e as msgs na fila serão consumidas tendo o corpo printado na tela. Também dá para avaliar os resultados no painel web.

Para realizar testes com vários envios abra uma nova aba/sessão do terminal, mantenha o consumidor em uma aba e na outra execute várias vezes o código producer e verifique os resultados.

## Encerrando o lab

1. Pare o consumer (receive.py) com CTRL+C.
2. Pare e remova o container do rabbit_mq

~~~bash
sudo docker container stop -t 0 my_rabbitmq
sudo docker container rm my_rabbitmq
~~~
