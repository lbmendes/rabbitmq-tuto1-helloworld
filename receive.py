import pika, sys, os

def main():
    '''Método principal do consumer. Roda em loop eterno o consumer, até que se aperte CTRL+C.'''

    # Cria conexão com broker localizado em localhost
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    
    
    # Declara uma fila com o nome de 'hello'
    # O comando de declaração de fila é idempotente,  ele garante que a fila irá existir.
    # Assim, caso fila já exista nada acontece, mas caso fila NÃO exista ele cria a fila.
    # OBS: Se rabbitmq tentar entregar msg para fila que não existe ele dropa a msg
    channel.queue_declare(queue='hello')
    
    
    # Cria método de callback que irá definir a manipulação de mensagem, neste exemplo somente dá um print
    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)
    
    
    # Declaramos o processo de "consume" da fila, chamando o nosso método de callback previamente criado
    channel.basic_consume(
        queue='hello',
        auto_ack=True,
        on_message_callback=callback
    )
    
    print(' [*] Waiting for messages. To exit press CTRL+C')
    
    # Inicia de fato o processo de "consume" da fila
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

